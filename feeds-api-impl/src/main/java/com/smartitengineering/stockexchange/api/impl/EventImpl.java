package com.smartitengineering.stockexchange.api.impl;

import com.google.appengine.api.datastore.Key;
import com.smartitengineering.stockexchange.api.Event;

/**
 *
 * @author imyousuf
 */
class EventImpl
    implements Event {

  private Action action;
  private EntityType entityType;
  private Object object;
  private Object oldObject;
  private Key objectKey;

  public void setAction(Action action) {
    this.action = action;
  }

  public void setEntityType(EntityType entityType) {
    this.entityType = entityType;
  }

  public void setObject(Object object) {
    this.object = object;
  }

  public void setObjectKey(Key objectKey) {
    this.objectKey = objectKey;
  }

  public void setOldObject(Object oldObject) {
    this.oldObject = oldObject;
  }

  public Action getAction() {
    return action;
  }

  public EntityType getEntityType() {
    return entityType;
  }

  public Object getObject() {
    return object;
  }

  public Object getOldObject() {
    return oldObject;
  }

  public Key getObjectKey() {
    return objectKey;
  }
}
