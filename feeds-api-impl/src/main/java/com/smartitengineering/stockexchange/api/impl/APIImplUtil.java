package com.smartitengineering.stockexchange.api.impl;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author imyousuf
 */
public final class APIImplUtil {

  private final static Logger log =
                              Logger.getLogger(APIImplUtil.class.getName());

  private APIImplUtil() {
  }
  private static Date nextExpirationDate;

  public static Date getNextExpirationDate() {
    if (nextExpirationDate == null) {
      initExpirationDate();
    }
    else {
      if (!nextExpirationDate.after(new Date())) {
        setNextDayAsExpirationDate();
      }
    }
    return nextExpirationDate;
  }

  private static synchronized void initExpirationDate() {
    if (nextExpirationDate != null) {
      return;
    }
    setNextDayAsExpirationDate();
  }

  private synchronized static void setNextDayAsExpirationDate() {
    if (nextExpirationDate != null && nextExpirationDate.after(new Date())) {
      return;
    }
    Calendar calendar =
             Calendar.getInstance(TimeZone.getTimeZone("GMT+0:00"));
    calendar.add(Calendar.DATE, 1);
    calendar.set(Calendar.HOUR_OF_DAY, 0);
    calendar.set(Calendar.MINUTE, 0);
    calendar.set(Calendar.SECOND, 0);
    nextExpirationDate = calendar.getTime();
    if (log.isLoggable(Level.FINEST)) {
      log.finest(new StringBuilder("Setting next expiration date to: ").append(
          nextExpirationDate).toString());
    }
  }
}
