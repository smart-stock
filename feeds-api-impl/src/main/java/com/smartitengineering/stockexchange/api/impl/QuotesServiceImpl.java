package com.smartitengineering.stockexchange.api.impl;

import com.smartitengineering.stockexchange.api.KeyPredictor;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Query;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.smartitengineering.stockexchange.api.Company;
import com.smartitengineering.stockexchange.api.DataConverter;
import com.smartitengineering.stockexchange.api.QuotesService;
import com.smartitengineering.stockexchange.api.StockExchange;
import com.smartitengineering.stockexchange.api.StockQuote;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author imyousuf
 */
@Singleton
public class QuotesServiceImpl
        implements QuotesService {

  private final DataConverter dataConverter;
  private final DatastoreService service;
  private final KeyPredictor predictor;
  private static final Logger log = Logger.getLogger(
          QuotesServiceImpl.class.getName());

  @Inject
  public QuotesServiceImpl(DataConverter dataConverter,
                           KeyPredictor predictor) {
    this.service = DatastoreServiceFactory.getDatastoreService();
    this.dataConverter = dataConverter;
    this.predictor = predictor;
  }

  public StockExchange getStockExchange(String symbolicName) {
    if (StringUtils.isBlank(symbolicName)) {
      final String errorMsg = "A blank symbol name was provided!";
      log.log(Level.WARNING, errorMsg);
      throw new IllegalArgumentException(errorMsg);
    }
    Key exchangeKey = getExchangeKey(symbolicName);
    StockExchange exchange;
    try {
      exchange = dataConverter.convertEntityToStockExchange(service.get(
              exchangeKey));
    }
    catch (EntityNotFoundException ex) {
      log.log(Level.INFO, ex.getMessage(), ex);
      exchange = null;
    }
    return exchange;
  }

  public Company getCompany(
          String companyTickr,
          StockExchange exchange) {
    if (StringUtils.isBlank(companyTickr) || exchange == null) {
      final String errorMsg =
                   "A blank company tickr or null exchange was provided!";
      log.log(Level.WARNING, errorMsg);
      throw new IllegalArgumentException(errorMsg);
    }
    final Key companyKey = getCompanyKey(exchange, companyTickr);
    Company company;
    try {
      company = dataConverter.convertEntityToCompany(service.get(companyKey),
              exchange);
    }
    catch (EntityNotFoundException ex) {
      log.log(Level.INFO, ex.getMessage(), ex);
      company = null;
    }
    return company;
  }

  public Map<Company, StockQuote> getQuotesMapForCompanyFromPersistentStorage(
          Company company) {
    Collection<StockQuote> quotes = getQuotesForCompanyFromPersistentStorage(
            company);
    return getStockQuoteMap(quotes);
  }

  public Collection<StockQuote> getQuotesForCompanyFromPersistentStorage(
          Company company) {
    Key companyKey = predictor.getCompanyKey(company.getExchange(), company);
    Query query = new Query(QuotesService.Kind.STOCK_QUOTES.name(), companyKey);
    Set<StockQuote> quotes = new LinkedHashSet<StockQuote>();
    Iterator<Entity> latestQuotes = service.prepare(query).asIterator();
    while (latestQuotes.hasNext()) {
      Entity result = latestQuotes.next();
      quotes.add(dataConverter.convertEntityToStockQuote(result, company));
    }
    return Collections.unmodifiableCollection(quotes);
  }

  public Map<Company, StockQuote> getQuotesMapForStockExchangeFromPersistentStorage(
          StockExchange exchange) {
    Collection<StockQuote> quotes = getQuotesForStockExchangeFromPersistentStorage(
            exchange);
    return getStockQuoteMap(quotes);
  }

  public Collection<StockQuote> getQuotesForStockExchangeFromPersistentStorage(
          StockExchange exchange) {
    Query latestQuoteQuery = new Query(QuotesService.Kind.STOCK_QUOTES_LATEST.
            name(), predictor.getExchangeKey(exchange));
    Set<StockQuote> quotes = new LinkedHashSet<StockQuote>();
    Iterator<Entity> latestQuotes =
                     service.prepare(latestQuoteQuery).asIterator();
    while (latestQuotes.hasNext()) {
      Entity result = latestQuotes.next();
      quotes.add(dataConverter.convertEntityToStockQuote(result, getCompany(
              (String) result.getProperty(StockQuote.PROP_COMPANY_TICKR),
              exchange)));
    }
    return Collections.unmodifiableCollection(quotes);
  }

  public Collection<StockExchange> getStockExchanges() {
    Query exchangesQuery = new Query(QuotesService.Kind.STOCK_EXCHANGE.name());
    Set<StockExchange> quotes = new LinkedHashSet<StockExchange>();
    Iterator<Entity> stockExchanges =
                     service.prepare(exchangesQuery).asIterator();
    while (stockExchanges.hasNext()) {
      Entity result = stockExchanges.next();
      quotes.add(
              dataConverter.convertEntityToStockExchange(result));
    }
    return quotes;
  }

  private Key getCompanyKey(final StockExchange exchange,
                            final String companyTickr) {
    return predictor.getCompanyKey(exchange, companyTickr);
  }

  private Key getExchangeKey(String symbolicName) {
    return predictor.getExchangeKey(symbolicName);
  }

  private Map<Company, StockQuote> getStockQuoteMap(
          Collection<StockQuote> quotes) {
    if (quotes == null) {
      return null;
    }
    else {
      if (quotes.isEmpty()) {
        return Collections.emptyMap();
      }
      else {
        Map<Company, StockQuote> quotesMap =
                                 new HashMap<Company, StockQuote>(quotes.size());
        for (StockQuote quote : quotes) {
          if (quote == null) {
            continue;
          }
          quotesMap.put(quote.getCompany(), quote);
        }
        return Collections.unmodifiableMap(quotesMap);
      }
    }
  }
}
