package com.smartitengineering.stockexchange.api.impl;

import com.smartitengineering.stockexchange.api.Company;
import com.smartitengineering.stockexchange.api.StockExchange;

/**
 *
 * @author imyousuf
 */
public class AbstractCacheImpl {

  final static String ALL_EXCHANGES =
                      "ALL_SMART_STOCK_REGISTERD_EXCHANGES";

  protected Object getCacheKey(final StockExchange exchange,
                               final Company company) {
    return getCacheKey(exchange, company.getSymbol());
  }

  protected Object getCacheKey(final StockExchange exchange,
                               final String companyTickr) {
    return new StringBuilder().append(exchange.getSymbolicName()).append(
            '-').append(companyTickr).toString();
  }

  protected Object getCompanyURLCacheKey(Company company) {
    return new StringBuilder("url-").append(company.getExchange().
            getSymbolicName()).append('-').append(company.getSymbol()).toString();
  }
}
