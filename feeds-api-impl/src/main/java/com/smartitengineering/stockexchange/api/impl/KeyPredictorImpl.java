package com.smartitengineering.stockexchange.api.impl;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.inject.Singleton;
import com.smartitengineering.stockexchange.api.Company;
import com.smartitengineering.stockexchange.api.KeyPredictor;
import com.smartitengineering.stockexchange.api.QuotesService;
import com.smartitengineering.stockexchange.api.StockExchange;

/**
 *
 * @author imyousuf
 */
@Singleton
public class KeyPredictorImpl
        implements KeyPredictor {

  public Key getCompanyKey(final StockExchange exchange,
                           final Company company) {
    return getCompanyKey(exchange, company.getSymbol());
  }

  public Key getCompanyKey(final StockExchange exchange,
                           final String companyTickr) {
    return KeyFactory.createKey(getExchangeKey(exchange),
            QuotesService.Kind.COMPANY.name(), companyTickr);
  }

  public Key getExchangeKey(final StockExchange exchange) {
    return getExchangeKey(exchange.getSymbolicName());
  }

  public Key getExchangeKey(final String symbolicName) {
    return KeyFactory.createKey(QuotesService.Kind.STOCK_EXCHANGE.name(),
            symbolicName);
  }

  public Key getLatestStockKey(StockExchange exchange,
                               Company company) {
    return getLatestStockKey(exchange, company.getSymbol());
  }

  public Key getLatestStockKey(StockExchange exchange,
                               String companyTickr) {
    return KeyFactory.createKey(getExchangeKey(exchange),
            QuotesService.Kind.STOCK_QUOTES_LATEST.name(), companyTickr);
  }
}
