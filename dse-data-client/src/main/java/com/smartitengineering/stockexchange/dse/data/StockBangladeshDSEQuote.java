package com.smartitengineering.stockexchange.dse.data;

/**
 *
 * @author imyousuf
 */
public class StockBangladeshDSEQuote {
  private int id;
  private String code;
  private String sector;
  private String category;
  private String market_lot;
  private String face_value;
  private Double lastprice;
  private Double high;
  private Double low;
  private long volume;
  private Double value;
  private long trade;
  private Double pchange;
  private Double pe;

  public String getCategory() {
    return category;
  }

  public void setCategory(String category) {
    this.category = category;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getFace_value() {
    return face_value;
  }

  public void setFace_value(String face_value) {
    this.face_value = face_value;
  }

  public Double getHigh() {
    return high;
  }

  public void setHigh(Double high) {
    this.high = high;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public Double getLastprice() {
    return lastprice;
  }

  public void setLastprice(Double lastprice) {
    this.lastprice = lastprice;
  }

  public Double getLow() {
    return low;
  }

  public void setLow(Double low) {
    this.low = low;
  }

  public String getMarket_lot() {
    return market_lot;
  }

  public void setMarket_lot(String market_lot) {
    this.market_lot = market_lot;
  }

  public Double getPe() {
    return pe;
  }

  public void setPe(Double pe) {
    this.pe = pe;
  }

  public Double getPchange() {
    return pchange;
  }

  public void setPchange(Double pchange) {
    this.pchange = pchange;
  }

  public String getSector() {
    return sector;
  }

  public void setSector(String sector) {
    this.sector = sector;
  }

  public long getTrade() {
    return trade;
  }

  public void setTrade(long trade) {
    this.trade = trade;
  }

  public Double getValue() {
    return value;
  }

  public void setValue(Double value) {
    this.value = value;
  }

  public long getVolume() {
    return volume;
  }

  public void setVolume(long volume) {
    this.volume = volume;
  }

  public String toString() {
    StringBuilder toStringBuilder = new StringBuilder();
    toStringBuilder.append(super.toString());
    toStringBuilder.append("\n");
    toStringBuilder.append("\nid: ");
    toStringBuilder.append(id);
    toStringBuilder.append("\ncode: ");
    toStringBuilder.append(code);
    toStringBuilder.append("\nsector: ");
    toStringBuilder.append(sector);
    toStringBuilder.append("\ncategory: ");
    toStringBuilder.append(category);
    toStringBuilder.append("\nmarket_lot: ");
    toStringBuilder.append(market_lot);
    toStringBuilder.append("\nface_value: ");
    toStringBuilder.append(face_value);
    toStringBuilder.append("\nlastprice: ");
    toStringBuilder.append(lastprice);
    toStringBuilder.append("\nhigh: ");
    toStringBuilder.append(high);
    toStringBuilder.append("\nlow: ");
    toStringBuilder.append(low);
    toStringBuilder.append("\nvolume: ");
    toStringBuilder.append(volume);
    toStringBuilder.append("\nvalue: ");
    toStringBuilder.append(value);
    toStringBuilder.append("\ntrade: ");
    toStringBuilder.append(trade);
    toStringBuilder.append("\npchange: ");
    toStringBuilder.append(pchange);
    toStringBuilder.append("\npe: ");
    toStringBuilder.append(pe);
    return toStringBuilder.toString();
  }
}
