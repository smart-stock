package com.smartitengineering.stockexchange.dse;

import com.smartitengineering.stockexchange.dse.data.PingResult;
import com.smartitengineering.stockexchange.dse.data.StockBangladeshDSEClient;
import java.util.Arrays;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        PingResult result = StockBangladeshDSEClient.getLatestQuotes();
        System.out.println(result == null ? "NULL!" : Arrays.toString(result.getResults()));

    }
}
