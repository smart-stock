package com.smartitengineering.stockexchange.feeds.ws;

import javax.ws.rs.ext.Provider;
import org.codehaus.jackson.jaxrs.Annotations;
import org.codehaus.jackson.jaxrs.JacksonJsonProvider;
import org.codehaus.jackson.map.ObjectMapper;

/**
 *
 * @author imyousuf
 */
@Provider
public class SmartStockJSONProvider
    extends JacksonJsonProvider {

  public SmartStockJSONProvider(ObjectMapper mapper,
                                Annotations[] annotationsToUse) {
    super(mapper, annotationsToUse);
  }

  public SmartStockJSONProvider(Annotations[] annotationsToUse) {
    super(annotationsToUse);
  }

  public SmartStockJSONProvider() {
  }
}
