package com.smartitengineering.stockexchange.feeds.ws.domain;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Date;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;

/**
 *
 * @author imyousuf
 */
public class StockQuote
    implements JsonProvider {

  private com.smartitengineering.stockexchange.api.StockQuote quote;
  private Company company;

  public StockQuote(com.smartitengineering.stockexchange.api.StockQuote quote) {
    if (quote == null) {
      throw new IllegalArgumentException();
    }
    this.quote = quote;
    company = new Company(quote.getCompany());
  }

  public Company getCompany() {
    return company;
  }

  public long getVolume() {
    return quote.getVolume();
  }

  public double getValue() {
    return quote.getValue();
  }

  public long getTrade() {
    return quote.getTrade();
  }

  public Date getQuoteTime() {
    return quote.getQuoteTime();
  }

  public double getLow() {
    return quote.getLow();
  }

  public double getLastPrice() {
    return quote.getLastPrice();
  }

  public double getHigh() {
    return quote.getHigh();
  }

  public double getChangePercentage() {
    return quote.getChangePercentage();
  }

  public com.smartitengineering.stockexchange.api.StockQuote toQuote() {
    return quote;
  }

  public void toJSON(final Writer writer,
                     final JsonGenerator parentGenerator)
      throws IOException {
    JsonFactory factory = new JsonFactory();
    final JsonGenerator generator;
    if (parentGenerator == null) {
      generator = factory.createJsonGenerator(writer);
    }
    else {
      generator = parentGenerator;
    }
    generator.writeStartObject();
    generator.writeObjectFieldStart("company");
    final String companyJson = getCompany().toJSON();
    generator.writeRaw(companyJson.substring(1, companyJson.length() - 1));
    generator.writeEndObject();
    generator.writeNumberField(
        com.smartitengineering.stockexchange.api.StockQuote.PROP_CHANGE_PERCENT,
        getChangePercentage());
    generator.writeNumberField(
        com.smartitengineering.stockexchange.api.StockQuote.PROP_HIGH,
        getHigh());
    generator.writeNumberField(
        com.smartitengineering.stockexchange.api.StockQuote.PROP_LOW,
        getLow());
    generator.writeNumberField(
        com.smartitengineering.stockexchange.api.StockQuote.PROP_LAST_PRICE,
        getLastPrice());
    generator.writeNumberField(
        com.smartitengineering.stockexchange.api.StockQuote.PROP_QUOTE_TIME,
        getQuoteTime().getTime());
    generator.writeNumberField(
        com.smartitengineering.stockexchange.api.StockQuote.PROP_TRADE,
        getTrade());
    generator.writeNumberField(
        com.smartitengineering.stockexchange.api.StockQuote.PROP_VALUE,
        getValue());
    generator.writeNumberField(
        com.smartitengineering.stockexchange.api.StockQuote.PROP_VOLUME,
        getVolume());
    generator.writeEndObject();
    if (parentGenerator == null) {
      generator.flush();
      generator.close();
    }
  }

  public String toJSON()
      throws IOException {
    StringWriter writer = new StringWriter();
    toJSON(writer, null);
    return writer.toString();
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final StockQuote other = (StockQuote) obj;
    if (this.quote != other.quote && (this.quote == null ||
                                      !this.quote.equals(other.quote))) {
      return false;
    }
    return true;
  }

  @Override
  public int hashCode() {
    int hash = 7;
    hash = 47 * hash + (this.quote != null ? this.quote.hashCode() : 0);
    return hash;
  }
}
