package com.smartitengineering.stockexchange.feeds;

import com.smartitengineering.stockexchange.feeds.panels.StockExchangePanel;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;

/**
 *
 * @author imyousuf
 */
public class HomePage
    extends WebPage {

  public HomePage() {
    add(new Label("hello", "Welcome to Smart Stock!"), new StockExchangePanel(
        "exchangeList"));
  }
}
