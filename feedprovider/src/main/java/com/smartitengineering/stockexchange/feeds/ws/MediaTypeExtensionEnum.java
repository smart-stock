package com.smartitengineering.stockexchange.feeds.ws;

import javax.ws.rs.core.MediaType;

/**
 *
 * @author imyousuf
 */
public enum MediaTypeExtensionEnum {

  atom(MediaType.APPLICATION_ATOM_XML_TYPE),
  htm(MediaType.TEXT_HTML_TYPE),
  html(MediaType.TEXT_HTML_TYPE),
  xml(MediaType.TEXT_XML_TYPE),
  json(MediaType.APPLICATION_JSON_TYPE),
  rss(MediaType.valueOf("application/rss+xml"));
  private final MediaType mediaType;

  MediaTypeExtensionEnum(MediaType mediaType) {
    this.mediaType = mediaType;
  }

  public MediaType getMediaType() {
    return mediaType;
  }
}
