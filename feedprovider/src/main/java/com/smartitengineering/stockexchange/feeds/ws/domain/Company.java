package com.smartitengineering.stockexchange.feeds.ws.domain;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;

/**
 *
 * @author imyousuf
 */
public class Company
    implements JsonProvider {

  private com.smartitengineering.stockexchange.api.Company apiCompany;

  public Company(com.smartitengineering.stockexchange.api.Company apiCompany) {
    this.apiCompany = apiCompany;
  }

  public String getSymbol() {
    return apiCompany.getSymbol();
  }

  public String getSector() {
    return apiCompany.getSector();
  }

  public int getLotSize() {
    return apiCompany.getLotSize();
  }

  public double getFaceValue() {
    return apiCompany.getFaceValue();
  }

  public String getDescription() {
    return apiCompany.getDescription();
  }

  public String getCategory() {
    return apiCompany.getCategory();
  }

  public com.smartitengineering.stockexchange.api.Company toCompany() {
    return apiCompany;
  }

  public void toJSON(final Writer writer,
                     final JsonGenerator parentGenerator)
      throws IOException {
    JsonFactory factory = new JsonFactory();
    final JsonGenerator generator;
    if (parentGenerator == null) {
      generator = factory.createJsonGenerator(writer);
    }
    else {
      generator = parentGenerator;
    }
    generator.writeStartObject();
    generator.writeStringField(
        com.smartitengineering.stockexchange.api.Company.PROP_SYMBOL,
        getSymbol());
    generator.writeStringField(
        com.smartitengineering.stockexchange.api.Company.PROP_SECT,
        getSector());
    generator.writeStringField(
        com.smartitengineering.stockexchange.api.Company.PROP_CAT,
        getCategory());
    generator.writeStringField(
        com.smartitengineering.stockexchange.api.Company.PROP_DESC,
        getDescription());
    generator.writeNumberField(
        com.smartitengineering.stockexchange.api.Company.PROP_FACE_VAL,
        getFaceValue());
    generator.writeNumberField(
        com.smartitengineering.stockexchange.api.Company.PROP_LOT_SIZE,
        getLotSize());
    generator.writeEndObject();
    if (parentGenerator == null) {
      generator.flush();
      generator.close();
    }
  }

  public String toJSON()
      throws IOException {
    StringWriter writer = new StringWriter();
    toJSON(writer, null);
    return writer.toString();
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final Company other = (Company) obj;
    if (this.apiCompany != other.apiCompany &&
        (this.apiCompany == null || !this.apiCompany.equals(other.apiCompany))) {
      return false;
    }
    return true;
  }

  @Override
  public int hashCode() {
    int hash = 3;
    hash =
    23 * hash + (this.apiCompany != null ? this.apiCompany.hashCode() : 0);
    return hash;
  }
}
