package com.smartitengineering.stockexchange.feeds.ws;

import com.sun.jersey.atom.rome.impl.provider.entity.AtomEntryProvider;
import com.sun.jersey.core.provider.AbstractMessageReaderWriterProvider;
import com.sun.syndication.feed.atom.Entry;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.Provider;

/**
 *
 * @author imyousuf
 */
@Provider
public class SmartStockAtomEntryProvider
    extends AbstractMessageReaderWriterProvider<Entry> {

  private final AtomEntryProvider entryProvider = new AtomEntryProvider();

  public boolean isReadable(Class<?> type,
                            Type genericType,
                            Annotation[] annotations,
                            MediaType mediaType) {
    return entryProvider.isReadable(type, genericType, annotations, mediaType);
  }

  public Entry readFrom(Class<Entry> type,
                        Type genericType,
                        Annotation[] annotations,
                        MediaType mediaType,
                        MultivaluedMap<String, String> httpHeaders,
                        InputStream entityStream)
      throws IOException,
             WebApplicationException {
    return entryProvider.readFrom(type, genericType, annotations, mediaType,
        httpHeaders, entityStream);
  }

  public boolean isWriteable(Class<?> type,
                             Type genericType,
                             Annotation[] annotations,
                             MediaType mediaType) {
    return entryProvider.isWriteable(type, genericType, annotations, mediaType);
  }

  public void writeTo(Entry t,
                      Class<?> type,
                      Type genericType,
                      Annotation[] annotations,
                      MediaType mediaType,
                      MultivaluedMap<String, Object> httpHeaders,
                      OutputStream entityStream)
      throws IOException,
             WebApplicationException {
    entryProvider.writeTo(t, type, genericType, annotations, mediaType,
        httpHeaders, entityStream);
  }
}
