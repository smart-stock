package com.smartitengineering.stockexchange.feeds.common;

/**
 *
 * @author imyousuf
 */
public enum ParamEnum {

  STOCK_EXCHANGE_PARAM {

    public String getParamName() {
      return "exchange_symbol";
    }
  };

  public abstract String getParamName();
}
