package com.smartitengineering.stockexchange.feeds.ws;

import com.sun.jersey.atom.rome.impl.provider.entity.AtomFeedProvider;
import com.sun.jersey.core.provider.AbstractMessageReaderWriterProvider;
import com.sun.syndication.feed.atom.Feed;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.Provider;

/**
 *
 * @author imyousuf
 */
@Provider
public class SmartStockAtomFeedProvider
    extends AbstractMessageReaderWriterProvider<Feed> {

  private final AtomFeedProvider feedProvider = new AtomFeedProvider();

  public boolean isReadable(Class<?> type,
                            Type genericType,
                            Annotation[] annotations,
                            MediaType mediaType) {
    return feedProvider.isReadable(type, genericType, annotations, mediaType);
  }

  public Feed readFrom(Class<Feed> type,
                       Type genericType,
                       Annotation[] annotations,
                       MediaType mediaType,
                       MultivaluedMap<String, String> httpHeaders,
                       InputStream entityStream)
      throws IOException,
             WebApplicationException {
    return feedProvider.readFrom(type, genericType, annotations, mediaType,
        httpHeaders, entityStream);
  }

  public boolean isWriteable(Class<?> type,
                             Type genericType,
                             Annotation[] annotations,
                             MediaType mediaType) {
    return feedProvider.isWriteable(type, genericType, annotations, mediaType);
  }

  public void writeTo(Feed t,
                      Class<?> type,
                      Type genericType,
                      Annotation[] annotations,
                      MediaType mediaType,
                      MultivaluedMap<String, Object> httpHeaders,
                      OutputStream entityStream)
      throws IOException,
             WebApplicationException {
    feedProvider.writeTo(t, type, genericType, annotations, mediaType,
        httpHeaders, entityStream);
  }
}
