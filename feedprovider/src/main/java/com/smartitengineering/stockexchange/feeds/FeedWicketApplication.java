package com.smartitengineering.stockexchange.feeds;

import com.smartitengineering.stockexchange.feeds.pages.StockExchangePage;
import org.apache.wicket.Application;
import org.apache.wicket.Page;
import org.apache.wicket.protocol.http.HttpSessionStore;
import org.apache.wicket.protocol.http.WebApplication;
import org.apache.wicket.request.target.coding.QueryStringUrlCodingStrategy;
import org.apache.wicket.session.ISessionStore;

/**
 *
 * @author imyousuf
 */
public class FeedWicketApplication
    extends WebApplication {

  @Override
  public Class<? extends Page> getHomePage() {
    return HomePage.class;
  }

  @Override
  protected void init() {
    super.init();
    getResourceSettings().setResourcePollFrequency(null);
    mount(new QueryStringUrlCodingStrategy("/exchange", StockExchangePage.class));
  }

  @Override
  protected ISessionStore newSessionStore() {
    return new HttpSessionStore(this);
  }

  @Override
  public String getConfigurationType() {
    return Application.DEPLOYMENT;
  }
}
