package com.smartitengineering.stockexchange.feeds.factory;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.smartitengineering.stockexchange.api.LatestQuoteService;
import com.smartitengineering.stockexchange.api.QuotesService;

/**
 *
 * @author imyousuf
 */
public class SmartStockFactory {

  private static Injector guiceInjector;
  private static SmartStockFactoryAPI internalAPI;

  protected static Injector getGuiceInjector() {
    if (guiceInjector == null) {
      return Guice.createInjector(new SmartStockFactoryModule());
    }
    return guiceInjector;
  }

  protected static void setGuiceInjector(Injector guiceInjector) {
    SmartStockFactory.guiceInjector = guiceInjector;
  }

  public static QuotesService getQuotesService() {
    return getAPI().getQuotesService();
  }

  public static LatestQuoteService getLatestQuoteService() {
    return getAPI().getLatestQuoteService();
  }

  public static SmartStockFactoryAPI getAPI() {
    if (internalAPI == null && guiceInjector == null) {
      internalAPI = new SmartStockFactoryModule.SmartStockFactory();
    }
    else if (internalAPI == null) {
      internalAPI = new SmartStockFactoryImpl();
    }
    return internalAPI;
  }

  private static class SmartStockFactoryImpl
      implements SmartStockFactoryAPI {

    public QuotesService getQuotesService() {
      return getGuiceInjector().getInstance(QuotesService.class);
    }

    public LatestQuoteService getLatestQuoteService() {
      return getGuiceInjector().getInstance(LatestQuoteService.class);
    }
  }
}
