package com.smartitengineering.stockexchange.feeds.ws;

import com.google.appengine.api.memcache.MemcacheService;
import com.google.appengine.api.memcache.MemcacheServiceFactory;
import com.smartitengineering.stockexchange.api.LatestQuoteService;
import com.smartitengineering.stockexchange.feeds.ws.domain.QuotesResource;
import com.smartitengineering.stockexchange.api.StockExchange;
import com.smartitengineering.stockexchange.feeds.factory.SmartStockFactory;
import com.smartitengineering.stockexchange.feeds.ws.domain.JsonProvider;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;
import org.apache.commons.lang.StringUtils;
import org.codehaus.jackson.map.ObjectMapper;

/**
 *
 * @author imyousuf
 */
@Path("/api/latest/{exchangeId}.json")
public class LatestQuotesResource {

  private final String exchangeId;
  private final MemcacheService memcacheService;
  private final String jsonCacheKey;
  private final static Logger log = Logger.getLogger(
      LatestQuotesResource.class.getName());

  public LatestQuotesResource(@Context UriInfo uriInfo,
                              @PathParam("exchangeId") String exchangeId) {
    if (StringUtils.isBlank(exchangeId)) {
      throw new IllegalArgumentException();
    }
    this.exchangeId = exchangeId;
    memcacheService = MemcacheServiceFactory.getMemcacheService();
    jsonCacheKey = new StringBuilder(LatestQuoteService.QUOTES_JSON_PREFIX).
        append(this.exchangeId).toString();
    if (log.isLoggable(Level.FINEST)) {
      log.log(Level.FINEST, new StringBuilder("ATOM Key class: ").append(
          jsonCacheKey.getClass()).append(" Key: ").append(jsonCacheKey).
          toString());
    }
  }

  @GET
  @Produces({MediaType.APPLICATION_JSON})
  public String getLatestQuotes() {
    final String cacheEntry = (String) memcacheService.get(jsonCacheKey);
    if (cacheEntry != null) {
      log.finest("Result from cache!");
      return cacheEntry;
    }
    StockExchange exchange = SmartStockFactory.getQuotesService().
        getStockExchange(exchangeId);
    if (exchange == null) {
      return null;
    }
    final QuotesResource quotesResource =
                         new QuotesResource(SmartStockFactory.getQuotesService().
        getQuotesForStockExchangeFromPersistentStorage(exchange));
    ObjectMapper mapper = new ObjectMapper();
    try {
      String value;
      try {
        if (quotesResource instanceof JsonProvider) {
          value = quotesResource.toJSON();
        }
        else {
          value = mapper.writeValueAsString(quotesResource);
        }
        memcacheService.put(jsonCacheKey, value);
      }
      catch(Exception ex) {
        log.log(Level.WARNING, ex.getMessage(), ex);
        value = "";
      }
      return value;
    }
    catch (Exception ex) {
      ex.printStackTrace();
      return "";
    }
  }
}
