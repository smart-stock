package com.smartitengineering.stockexchange.feeds.ws;

import com.google.appengine.api.memcache.MemcacheService;
import com.google.appengine.api.memcache.MemcacheServiceFactory;
import com.smartitengineering.stockexchange.api.LatestQuoteService;
import com.smartitengineering.stockexchange.feeds.ws.domain.QuotesResource;
import com.smartitengineering.stockexchange.api.StockExchange;
import com.smartitengineering.stockexchange.feeds.factory.SmartStockFactory;
import com.sun.syndication.feed.rss.Channel;
import com.sun.syndication.io.WireFeedOutput;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author imyousuf
 */
@Path("/api/latest/{exchangeId}.rss")
public class LatestQuotesFeedResource {

  private final String exchangeId;
  private final MemcacheService memcacheService;
  private final String atomCacheKey;
  private final UriInfo uriInfo;
  private final static Logger log = Logger.getLogger(
          LatestQuotesFeedResource.class.getName());

  public LatestQuotesFeedResource(@Context UriInfo uriInfo,
                                  @PathParam("exchangeId") String exchangeId) {
    if (StringUtils.isBlank(exchangeId)) {
      throw new IllegalArgumentException();
    }
    this.exchangeId = exchangeId;
    memcacheService = MemcacheServiceFactory.getMemcacheService();
    atomCacheKey = new StringBuilder(LatestQuoteService.QUOTES_ATOM_PREFIX).
            append(this.exchangeId).toString();
    this.uriInfo = uriInfo;
    if (log.isLoggable(Level.FINEST)) {
      log.log(Level.FINEST, new StringBuilder("ATOM Key class: ").append(
              atomCacheKey.getClass()).append(" Key: ").append(atomCacheKey).
              toString());
    }
  }

  @GET
  @Produces({"application/rss+xml"})
  public String getLatestQuotesFeed() {
    final String cacheEntry = (String) memcacheService.get(atomCacheKey);
    if (cacheEntry != null) {
      log.finest("Result from cache!");
      return cacheEntry;
    }
    StockExchange exchange = SmartStockFactory.getQuotesService().
            getStockExchange(exchangeId);
    if (exchange == null) {
      return null;
    }
    final QuotesResource quotesResource =
                         new QuotesResource(SmartStockFactory.getQuotesService().
            getQuotesForStockExchangeFromPersistentStorage(exchange));
    try {
      Channel feed = quotesResource.toChannel(uriInfo);
      final String value = new WireFeedOutput().outputString(feed);
      memcacheService.put(atomCacheKey, value);
      return value;
    }
    catch (Exception ex) {
      ex.printStackTrace();
      return "";
    }
  }
}
