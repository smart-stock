package com.smartitengineering.stockexchange.feeds.factory;

import com.google.inject.servlet.ServletModule;
import com.smartitengineering.stockexchange.feeds.SmartStockWicketFilter;
import com.smartitengineering.stockexchange.feeds.cron.DataPopulator;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author imyousuf
 */
class SmartStockWebAppModule
    extends ServletModule {

  @Override
  protected void configureServlets() {
    serve("/cron/StockDataPopulator").with(DataPopulator.class);
    Map<String, String> params = new HashMap<String, String>();
  }
}
