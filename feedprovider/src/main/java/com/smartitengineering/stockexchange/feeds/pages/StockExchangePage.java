package com.smartitengineering.stockexchange.feeds.pages;

import com.smartitengineering.stockexchange.api.StockQuote;
import com.smartitengineering.stockexchange.feeds.common.ParamEnum;
import com.smartitengineering.stockexchange.feeds.factory.SmartStockFactory;
import java.util.Collection;
import org.apache.wicket.PageParameters;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;

/**
 *
 * @author imyousuf
 */
public class StockExchangePage
    extends WebPage {

  public StockExchangePage(PageParameters parameters) {
    super(parameters);
    final String[] seSymbol =
        (String[]) parameters.get(ParamEnum.STOCK_EXCHANGE_PARAM.getParamName());
    final String name = seSymbol[0];
    add(new Label("hello", new StringBuilder("").
        append(SmartStockFactory.getQuotesService().getStockExchange(name).getName()).toString()));
    add(new Label("symbol", name));    
  }
}
