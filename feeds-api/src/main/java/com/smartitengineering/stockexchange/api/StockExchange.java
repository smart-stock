package com.smartitengineering.stockexchange.api;

/**
 *
 * @author imyousuf
 */
public class StockExchange
        extends AbstractDomain {

  public static final String PROP_SYMBOLIC_NAME = "symbolicName";
  public static final String PROP_NAME = "name";
  public static final String PROP_WEB_URL = "websiteUrl";
  public static final String PROP_ADDRESS = "address";
  public static final String PROP_TRADING_START_TIME = "tradingStartTime";
  public static final String PROP_TRADING_END_TIME = "tradingEndTime";
  public static final String PROP_ID = "exchangeId";
  private String symbolicName;
  private String name;
  private String websiteUrl;
  private String address;
  private String tradingStartTime;
  private String tradingEndTime;

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public String getSymbolicName() {
    return symbolicName;
  }

  public void setSymbolicName(String symbolicName) {
    this.symbolicName = symbolicName;
  }

  public String getTradingEndTime() {
    return tradingEndTime;
  }

  public void setTradingEndTime(String tradingEndTime) {
    this.tradingEndTime = tradingEndTime;
  }

  public String getTradingStartTime() {
    return tradingStartTime;
  }

  public void setTradingStartTime(String tradingStartTime) {
    this.tradingStartTime = tradingStartTime;
  }

  public String getWebsiteUrl() {
    return websiteUrl;
  }

  public void setWebsiteUrl(String websiteUrl) {
    this.websiteUrl = websiteUrl;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final StockExchange other = (StockExchange) obj;
    if ((this.symbolicName == null) ? (other.symbolicName != null)
        : !this.symbolicName.equals(other.symbolicName)) {
      return false;
    }
    return true;
  }

  @Override
  public int hashCode() {
    int hash = 3;
    hash = 59 * hash + (this.symbolicName != null ? this.symbolicName.hashCode()
                        : 0);
    return hash;
  }
}
