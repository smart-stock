package com.smartitengineering.stockexchange.api;

import java.util.Collection;

/**
 *
 * @author imyousuf
 */
public interface ExternalDataService {

  public Collection<StockQuote> getLatestLiveStockQuotes();

  public String getStockExchangeSymbol();

  public StockExchange getRawStockExchange();

  public String getCompanyUrl(Company company);
}
