package com.smartitengineering.stockexchange.api;

import com.google.appengine.api.datastore.Entity;

/**
 *
 * @author imyousuf
 */
public interface DataConverter {

  public Entity convertStockExchangeToEntity(StockExchange exchange);

  public Entity convertCompanyToEntity(Company company);

  public Entity convertStockQuoteToEntity(StockQuote quote);

  public Company convertEntityToCompany(Entity entity,
                                        StockExchange exchange);

  public StockExchange convertEntityToStockExchange(Entity entity);

  public StockQuote convertEntityToStockQuote(Entity entity,
                                              Company company);
}
