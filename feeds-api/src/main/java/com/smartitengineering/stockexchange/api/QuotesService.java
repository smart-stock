package com.smartitengineering.stockexchange.api;

import java.util.Collection;
import java.util.Map;

/**
 *
 * @author imyousuf
 */
public interface QuotesService {

  public enum Kind {

    STOCK_QUOTES,
    STOCK_QUOTES_LATEST,
    STOCK_EXCHANGE,
    COMPANY
  }

  public Collection<StockExchange> getStockExchanges();

  public StockExchange getStockExchange(String symbolicName);

  public Company getCompany(String companyTickr,
                            StockExchange exchange);

  public Collection<StockQuote> getQuotesForCompanyFromPersistentStorage(
          Company company);

  public Collection<StockQuote> getQuotesForStockExchangeFromPersistentStorage(
          StockExchange exchange);

  public Map<Company, StockQuote> getQuotesMapForStockExchangeFromPersistentStorage(
          StockExchange exchange);
}
