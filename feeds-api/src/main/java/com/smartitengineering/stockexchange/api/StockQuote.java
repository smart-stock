package com.smartitengineering.stockexchange.api;

import java.util.Date;

/**
 *
 * @author imyousuf
 */
public class StockQuote
        extends AbstractDomain {

  public static final String PROP_VERSION = "version";
  public static final String PROP_COMPANY_TICKR = "companyTickr";
  public static final String PROP_STOCK_XCHANGE_SYBMOL = "xchangeSymbol";
  public static final String PROP_ID = "quoteId";
  public static final String PROP_LAST_PRICE = "lastPrice";
  public static final String PROP_QUOTE_TIME = "quoteTime";
  public static final String PROP_HIGH = "high";
  public static final String PROP_LOW = "low";
  public static final String PROP_VOLUME = "volume";
  public static final String PROP_VALUE = "value";
  public static final String PROP_TRADE = "trade";
  public static final String PROP_CHANGE_PERCENT = "changePercentage";
  private long version;
  private Company company;
  private Date quoteTime;
  private Double lastPrice;
  private double high;
  private double low;
  private long volume;
  private double value;
  private long trade;
  private double changePercentage;

  public double getChangePercentage() {
    return changePercentage;
  }

  public void setChangePercentage(double changePercentage) {
    this.changePercentage = changePercentage;
  }

  public Company getCompany() {
    return company;
  }

  public void setCompany(Company company) {
    this.company = company;
  }

  public double getHigh() {
    return high;
  }

  public void setHigh(double high) {
    this.high = high;
  }

  public double getLastPrice() {
    return lastPrice;
  }

  public void setLastPrice(double lastPrice) {
    this.lastPrice = lastPrice;
  }

  public double getLow() {
    return low;
  }

  public void setLow(double low) {
    this.low = low;
  }

  public Date getQuoteTime() {
    return quoteTime;
  }

  public void setQuoteTime(Date quoteTime) {
    this.quoteTime = quoteTime;
  }

  public long getTrade() {
    return trade;
  }

  public void setTrade(long trade) {
    this.trade = trade;
  }

  public double getValue() {
    return value;
  }

  public void setValue(double value) {
    this.value = value;
  }

  public long getVolume() {
    return volume;
  }

  public void setVolume(long volume) {
    this.volume = volume;
  }

  public long getVersion() {
    return version;
  }

  public void setVersion(long version) {
    this.version = version;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final StockQuote other = (StockQuote) obj;
    if (this.company != other.company &&
        (this.company == null || !this.company.equals(other.company))) {
      return false;
    }
    if (this.lastPrice != other.lastPrice &&
        (this.lastPrice == null || !this.lastPrice.equals(other.lastPrice))) {
      return false;
    }
    return true;
  }

  @Override
  public int hashCode() {
    int hash = 3;
    hash = 97 * hash + (this.company != null ? this.company.hashCode() : 0);
    hash = 97 * hash + (this.quoteTime != null ? this.lastPrice.hashCode() : 0);
    return hash;
  }
}
