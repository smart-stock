package com.smartitengineering.stockexchange.api;

import com.google.appengine.api.datastore.Key;

/**
 *
 * @author imyousuf
 */
public interface Event<T> {

  public enum Action {

    CREATE,
    UPDATE,
    DELETE
  }

  public enum EntityType {

    STOCK_EXCHANGE,
    COMPANY,
    STOCK_QUOTE
  }

  public Action getAction();

  public EntityType getEntityType();

  public T getObject();

  public T getOldObject();

  public Key getObjectKey();
}
