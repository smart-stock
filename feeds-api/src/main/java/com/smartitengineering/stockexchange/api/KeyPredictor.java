package com.smartitengineering.stockexchange.api;

import com.google.appengine.api.datastore.Key;

/**
 *
 * @author imyousuf
 */
public interface KeyPredictor {

  Key getLatestStockKey(final StockExchange exchange,
                    final Company company);

  Key getLatestStockKey(final StockExchange exchange,
                    final String companyTickr);

  Key getCompanyKey(final StockExchange exchange,
                    final Company company);

  Key getCompanyKey(final StockExchange exchange,
                    final String companyTickr);

  Key getExchangeKey(final StockExchange exchange);

  Key getExchangeKey(final String symbolicName);
}
